# <center> API para reconhecimento Facial com ênfase em identificar a idade </center>
<center><a href="https://www.linkedin.com/in/eliassgarcia/"><img alt="Feito por" src="https://img.shields.io/badge/Feito%20por-Elias%20Garcia-brightgreen"></a>  <a href="https://www.linkedin.com/in/eliassgarcia/"><img alt="Feito por" src="https://img.shields.io/badge/Tecnologia-.NET%205.0-blueviolet"></a></center>



## Descubra se seu rosto corresponde à sua idade
--- 

<center><a href= "#computer-Sobre-a-api" >Sobre a API</a> | <a href="#memo-funcionalidades">O que fazemos</a> | <a href="#mortar_board-tecnologias-utilizadas">O que utilizamos</a> | <a href="#mag-como-utilizar">Como utilizar</a></center>

<br></br>
## :computer: Sobre a API
---
Usando como base uma API externa da [Cloudmersive](https://cloudmersive.com/image-recognition-and-processing-api), e construída totalmente em C#, consegue-se a partir da foto de qualquer pessoa, identificar a idade dela, que após ser processada, é salva no banco de dados, relacionada ao usuário que fez o upload.

O projeto Recognize está dividido em módulos:

| Módulo         | Descrição                                            |
| -------------- | ---------------------------------------------------- |
| Recognize.API  | Ponto de entrada da aplicação (Controllers)          |
| Recognize.Core | Servicos, Entidades, Regras da API                   |
| Recognize.Data | Camada de acesso a dados, conexão com bando de dados |


<br></br>
## :memo: Funcionalidades
---
As funções implementadas nesta API foram separadas por níveis de acesso, cada uma com sua respectiva funcionalidade, sendo elas:
### :unlock: Público
- Ver as 3 últimas imagens postadas na plataforma
- Realizar login
- Informações sumarizadas

### :busts_in_silhouette: Usuário
- Enviar uma imagem para análise
- Ver uma lista de todas as imagens que ele enviou
- Alterar a senha
- Apagar uma imagem enviada

### :construction: Gestor
- Adicionar usuários
- Listar todos os usuários cadastrados
- Remover um usuário e todos os dados relaicionados a ele

<br></br>
## :mortar_board: Tecnologias Utilizadas
---
- [.NET 5.0 SDK](https://dotnet.microsoft.com/download/dotnet/5.0)
- [JWT](https://jwt.io)
- [API de processamento e reconhecimento de imagem e rosto](https://cloudmersive.com/image-recognition-and-processing-api)
<br></br>
## :wrench: Ferramentas Utilizadas
---
- [Insomnia](https://insomnia.rest/download/)
- [PostgreSQL](https://www.postgresql.org/)
- [VSCode](https://code.visualstudio.com/)
- [Visual Studio Community 2019](https://visualstudio.microsoft.com/pt-br/vs/)

<br></br>
## :mag: Como Utilizar
---
### Requerimentos

- [Insomnia](https://insomnia.rest/download/) ou [Postman](https://www.postman.com/downloads/) para enviar as requisições
- [.NET 5.0 SDK](https://dotnet.microsoft.com/download/dotnet/5.0) para compilar e executar a aplicação
---

### Compilando

**Utilizando o Visual Studio**

1. Clone o projeto para sua máquina local.

2. Abra o arquivo _Recognize.sln_ com o Visual Studio.

3. Execute a aplicação a partir do projeto Recognize.API.

> No navegador padrão da máquina será aberto uma página com o sistema, Utilize a url `<base_url:port>/swagger` e verifique se o projeto está iniciando normalmente.

- **Caso esteja utilizando o Visual Studio Code**

> Neste momento o seu Visual Studio Code e o .NET Core devem estar devidamente instalados na máquina.

1. Clone o projeto para sua máquina local.

2. Abra a pasta do projeto no editor de código.

3. Aceite todas as sugestões de instalação de extensões que aparecerem assim que a pasta do projeto for aberta.

4. No menu Debug, inicie o debug do projeto

> No navegador padrão da máquina será aberto uma página com o sistema, Utilize a url `<base_url:port>/swagger` e verifique se o projeto está iniciando normalmente.

<br>

:grey_exclamation: Este projeto usa um banco de dados local, então você pode importar os dados do banco de dados usando o arquivo _sqlfile.sql_ localizado na raiz do projeto, enquanto não utilizamos um banco remoto!

 

---
<br></br>
### :bookmark: JSONs necessários para as requisições
---
- Para fazer o login, envie um post para `<base_url:port>/api/publico/login` com o seguinte JSON:
```json
{
    "login" : "login",
    "password" : "senha_cadastrada"
}
```
- A API vai te retornar um token, que deve ser colocado no value do Header Authorization, precedido por Bearer


<center><img alt= "Exemplo Header"src="https://i.ibb.co/zbbJb46/Authorization.jpg"></center>


- Para enviar uma imagem para ser analisada, é só enviar o caminho aonde a imagem está localizada (Duplicar as barras para evitar erros no JSON), para a seguinte URL: `<base_url:port>/api/usuario/{id}/upload` aonde {id} é o id do usuário logado

```json
{
    "caminho" : "C:\\Caminho_da_pasta\\arquivo.jpg"

}
```

No caso dos gestores da API, eles podem adicionar e remover usuários, para adicionar, usar o seguinte JSON na URL `<base_url:port>/api/gestor/criar_usuario`:

```json
{
    "nome": "Nome Completo",
	"login": "login_unico",
	"password": "senha para login",
	"role": "gestor/usuario"

}
```

Requisições GET e DELETE não necessitam de um JSON no corpo, sendo passadas diretamente na URL

### :globe_with_meridians: Métodos GET:
- `/api/publico` para receber uma lista das 3 últimas imagens cadastradas
- `/api/info` para receber informação da quantidade de imagens e usuários cadastrados
- `/api/usuario/{id}/list` para receber uma lista de imagens cadastradas pelo usuário
- `/api/gestor/list` para receber uma lista dos usuários cadastrados

### :globe_with_meridians: Meétodos DELETE
- `/api/usuario/{id_usuario}/delete/{id_imagem}` para deletar uma imagem cadastrada por um determinado usuário
  
- `/api/gestor/remover_usuario/3` para remover um usuário específico

<br>

URLs com `/api/usuario` são restritas a usuários com regra _usuario_
URLs com `/api/gestor` são exclusivas para gestores

  <br></br>
  <br></br>
  <br></br>
Sendo feito com carinho e dedicação por [Elias Garcia](https://gitlab.com/t1sbita). </br>
Dicas e opiniões construtivas serão sempre bem vindas!! :pray:
