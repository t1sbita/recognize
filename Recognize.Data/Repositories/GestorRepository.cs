﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Recognize.Core.Exceptions;
using Recognize.Core.Interfaces;
using Recognize.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Recognize.Data.Repositories
{
    public class GestorRepository : IGestorRepository
    {
        Usuario usuario = new Usuario();
        private readonly DataContext _context;
        public GestorRepository(DataContext context)
        {
            _context = context;
        }

        public string CriarUsuario(Usuario usuario)
        {
            try
            {
                _context.Usuario.Add(new Usuario
                {
                    Nome = usuario.Nome,
                    Login = usuario.Login,
                    Password = usuario.Password,
                    Role = usuario.Role,
                    DataUpload = DateTime.Now,
                    Idade = 0
                });
                _context.SaveChanges();
                return JsonConvert.SerializeObject(usuario);
            }
            catch (MinhasExcecoes)
            {

                return HttpStatusCode.BadRequest.ToString();
            }

        }
        public IActionResult RemoverUsuario(int idUsuario)
        {
            try
            {
                usuario = _context.Usuario.FirstOrDefault(x => x.Id == idUsuario);
                _context.Usuario.Remove(usuario);
                List<Imagem> listaImagens = new List<Imagem>();
                listaImagens = _context.Imagem.Where(x => x.UsuarioId == idUsuario).ToList();
                _context.Imagem.RemoveRange(listaImagens);
                _context.SaveChanges();
                return new OkObjectResult("Usuário removido!");

            }
            catch (Exception)
            {
                return new BadRequestObjectResult("Usuario não encontrado!");
            }


        }
        public List<Usuario> ListarUsuarios()
        {
            return _context.Usuario.ToList();
        }
        public bool VerificaExistente(Usuario usuario)
        {
            return (_context.Usuario.FirstOrDefault(x => x.Login == usuario.Login) != null);

        }

    }
}
