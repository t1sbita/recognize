﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Interfaces;
using Recognize.Core.Models;
using Recognize.Data.ConectToApiExterna;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recognize.Data.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly DataContext _context;
        Imagem imagem = new Imagem();
        Usuario usuario = new Usuario();
        RecognizeFaceService recognizeFace = new RecognizeFaceService();

        public UsuarioRepository(DataContext context)
        {
            _context = context;
        }

        public Imagem EnviarImagem(Imagem model, int id)
        {
            var dados = recognizeFace.ReconhecerRosto(model.Caminho);
            if (dados == null) return null;
            if (dados.PeopleIdentified > 0)
            {
                usuario = _context.Usuario.FirstOrDefault(x => x.Id == id);
                usuario.Idade = (int)dados.PeopleWithAge[0].Age;
                usuario.DataUpload = DateTime.Now;


                imagem = new Imagem
                {
                    Caminho = model.Caminho,
                    DataUpload = DateTime.Now,
                    Usuario = usuario,
                    Idade = usuario.Idade
                };
                _context.Imagem.Add(imagem);

                usuario.ListaImagem.Add(imagem);
                _context.SaveChanges();

                return imagem;
            }
            return null;
        }

        public Usuario AtualizarSenha(int id, Usuario model)
        {
            try
            {
                var usuario = _context.Usuario.FirstOrDefault(x => x.Id == id);
                if (usuario == null) return null;

                _context.Usuario.Attach(usuario);
                usuario.Password = model.Password;
                _context.SaveChanges();
                return usuario;
            }
            catch (NullReferenceException)
            {

                throw new NullReferenceException("Erros de informação");
            }


        }


        public List<Imagem> ListaImagem(int id)
        {
            List<Imagem> listaImagem = _context.Imagem.Where(x => x.Usuario.Id == id).ToList();
            
            return listaImagem;

        }


        public IActionResult ApagarFoto(int idUsuario, int idImagem)
        {
            imagem = _context.Imagem.SingleOrDefault(x => x.Id == idImagem);

            if (imagem == null)
            {
                return new NotFoundResult();
            }

            if (imagem.UsuarioId != idUsuario)
            {
                return new NotFoundObjectResult("Imagem não foi cadastrada por este usuário");
            }
            else
            {
                _context.Imagem.Remove(imagem);
                _context.SaveChanges();
                return new OkObjectResult("Imagem removida do banco de dados!");

            }

        }
        public string RetornaLogin(int id)
        {
            usuario = _context.Usuario.FirstOrDefault(x => x.Id == id);
            if (usuario != null)
            {
                return usuario.Login;
            }
            return null;
        }
    }
}
