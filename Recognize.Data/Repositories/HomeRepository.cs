﻿using Recognize.Core.Interfaces;
using Recognize.Core.Models;
using Recognize.Core.Security;
using System.Collections.Generic;
using System.Linq;


namespace Recognize.Data.Repositories
{
    public class HomeRepository : IHomeRepository
    {
        private readonly DataContext _context;
        public HomeRepository(DataContext context)
        {
            _context = context;
        }
        public HomeRepository()
        {

        }

        public string Autenticar(string login, string password)
        {
            var user = _context.Usuario.Where(x => x.Login == login).FirstOrDefault(x => x.Password == password);

            if (user == null)
                return null;

            var token = TokenService.GenerateToken(user);
            user.Password = "";
            return token;
        }

        public List<Imagem> ListaPublica()
        {   
            List<Imagem> listaImagem = _context.Imagem.OrderByDescending(x => x.DataUpload).Take(3).ToList();
            
            return listaImagem;
        }
        public object InfoGeral()
        {
            object modelHome = new object();
            int qtdImagens = _context.Imagem.Count();
            int qtdUsuarios = _context.Usuario.Count();
            int idadeTotal = 0;

            foreach (Usuario usuario in _context.Usuario)
            {
                idadeTotal += usuario.Idade;
            }

            modelHome = new 
            {
                QuantidadeImagens = qtdImagens,
                QuantidadeUsuarios = qtdUsuarios,
                IdadeMedia = idadeTotal / qtdUsuarios

            };
            return modelHome;
        }
    }
}
