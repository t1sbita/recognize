﻿using Cloudmersive.APIClient.NETCore.ImageRecognition.Api;
using Cloudmersive.APIClient.NETCore.ImageRecognition.Client;
using Cloudmersive.APIClient.NETCore.ImageRecognition.Model;
using System;
using System.Diagnostics;
using System.IO;

namespace Recognize.Data.ConectToApiExterna
{
    public class RecognizeFaceService
    {
        public AgeDetectionResult ReconhecerRosto(string patch)
        {
            Configuration.Default.AddApiKey("Apikey", "97b21fed-997f-468b-89ae-4764e3040c95");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("Apikey", "Bearer");

            var apiInstance = new FaceApi();
            FileStream arquivoImagem = null;
            //Verifica se o caminho existe
            if (File.Exists(patch))
            {
                // System.IO.Stream | Image file to perform the operation on.  Common file formats such as PNG, JPEG are supported.
                arquivoImagem = new FileStream(patch, FileMode.Open);
            }
            else
            {
                return null;
            }

            try
            {
                // Transform an image into an artistic painting automatically
                AgeDetectionResult result = apiInstance.FaceDetectAge(arquivoImagem);
                return result;
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception durante chamada da API Externa: " + e.Message);
                return null;
            }

        }
    }
}
