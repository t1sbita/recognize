﻿using Microsoft.EntityFrameworkCore;
using Recognize.Core.Models;

namespace Recognize.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Gestor> Gestor { get; set; }
        public DbSet<Imagem> Imagem { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=projeto_final;Username=postgres;Password=elias1993");
        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
    }
}
