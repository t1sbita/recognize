﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Interfaces;
using Recognize.Core.Models;
using Recognize.Core.Services.Interfaces;
using System.Collections.Generic;

namespace Recognize.Core.Services
{
    public class GestorService : IGestorService
    {
        private readonly IGestorRepository _gestorRepository;

        public GestorService(IGestorRepository gestorRepository)
        {
            _gestorRepository = gestorRepository;

        }

        public List<Usuario> ListarUsuarios()
        {
            return _gestorRepository.ListarUsuarios();
        }

        public IActionResult AdicionarUsuario(Usuario usuario)
        {
            if (usuario.Role != "usuario" && usuario.Role != "gestor")
            {
                return new BadRequestObjectResult("Regra de acesso incorreta!");
            }
            if (_gestorRepository.VerificaExistente(usuario))
            {
                return new BadRequestObjectResult("Usuário já existe");
            }

            var resultado = _gestorRepository.CriarUsuario(usuario);
            if (resultado == "BadRequest")
            {
                return new BadRequestObjectResult("Dados Incorretos");
            }
            else
            {
                return new ObjectResult(resultado);
            }

        }

        public IActionResult RemoverUsuario(int usuario)
        {
            return _gestorRepository.RemoverUsuario(usuario);
        }
    }
}
