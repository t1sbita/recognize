﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Interfaces;
using Recognize.Core.Models;
using Recognize.Core.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recognize.Core.Services
{
    public class HomeService : IHomeService
    {
        private readonly IHomeRepository _homeRepository;

        public HomeService(IHomeRepository homeRepository)
        {
            _homeRepository = homeRepository;

        }

        public async Task<ActionResult<dynamic>> Autenticar(Usuario usuario)
        {
            string token = _homeRepository.Autenticar(usuario.Login, usuario.Password);
            if (token == null)
            {
                return null;

            }

            return new
            {
                token = token
            };
        }

        public List<Imagem> ListaPublica()
        {
            return _homeRepository.ListaPublica();
        }

        public object InfoGeral()
        {
            return _homeRepository.InfoGeral();
 
        }
    }
}
