﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recognize.Core.Services.Interfaces
{
    public interface IHomeService
    {
        public Task<ActionResult<dynamic>> Autenticar(Usuario usuario);
        public List<Imagem> ListaPublica();
        public object InfoGeral();
    }
}
