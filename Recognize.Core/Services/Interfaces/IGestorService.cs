﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Models;
using System.Collections.Generic;

namespace Recognize.Core.Services.Interfaces
{
    public interface IGestorService
    {
        public List<Usuario> ListarUsuarios();
        public IActionResult AdicionarUsuario(Usuario usuario);
        public IActionResult RemoverUsuario(int usuario);


    }
}
