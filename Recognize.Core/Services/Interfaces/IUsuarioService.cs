﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Models;
using System.Collections.Generic;

namespace Recognize.Core.Services.Interfaces
{
    public interface IUsuarioService
    {
        public Imagem EnviarImagem(Imagem caminho, int id);
        public List<Imagem> ListaImagens(int id);
        public IActionResult AtualizaSenha(int id, Usuario senha);
        public IActionResult ApagarImagem(int id, int imagem);
        public string RetornaLogin(int id);
    }
}
