﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Recognize.Core.Exceptions;
using Recognize.Core.Interfaces;
using Recognize.Core.Models;
using Recognize.Core.Services.Interfaces;
using System.Collections.Generic;

namespace Recognize.Core.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;

        }

        public List<Imagem> ListaImagens(int id)
        {
            var listaImagens = _usuarioRepository.ListaImagem(id);

            if (listaImagens.Count > 0)
            {
                return listaImagens; 
            }
            else
            {
                return null;
            }

        }

        public Imagem EnviarImagem(Imagem caminho, int id)
        {
            return _usuarioRepository.EnviarImagem(caminho, id);
            
        }

        public IActionResult AtualizaSenha(int id, Usuario senha)
        {
            if (senha == null)
            {
                return new BadRequestObjectResult("Dados incorretos!");
            }
            return new OkObjectResult(_usuarioRepository.AtualizarSenha(id, senha));
        }

        public IActionResult ApagarImagem(int id, int imagem)
        {

            return _usuarioRepository.ApagarFoto(id, imagem);
        }

        public string RetornaLogin(int id)
        {
            return _usuarioRepository.RetornaLogin(id);
        }
    }
}
