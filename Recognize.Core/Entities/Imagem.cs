﻿using System;

namespace Recognize.Core.Models
{
    public class Imagem
    {
        public int Id { get; set; }
        public Usuario Usuario { get; set; }
        public int UsuarioId { get; set; }
        public DateTime DataUpload { get; set; }
        public string Caminho { get; set; }
        public int Idade { get; set; }

    }
}
