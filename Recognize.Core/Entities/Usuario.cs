﻿
using System;
using System.Collections.Generic;

namespace Recognize.Core.Models
{
    public class Usuario : Pessoas
    {
        public int Idade { get; set; }

        public DateTime DataUpload { get; set; }

        public List<Imagem> ListaImagem { get; set; }

        public Usuario()
        {

        }

    }
}
