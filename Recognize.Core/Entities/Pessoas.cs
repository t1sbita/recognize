﻿using System.ComponentModel.DataAnnotations;

namespace Recognize.Core.Models
{
    public class Pessoas
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Role { get; set; }

    }
}
