﻿using System;


namespace Recognize.Core.Exceptions
{
    public class MinhasExcecoes : Exception
    {
        public Exception Exception { get; set; }
        public MinhasExcecoes()
        {

        }

        public MinhasExcecoes(string mensagem) : base(String.Format("Operação inválida: {0}", mensagem))
        {

        }
    }
}
