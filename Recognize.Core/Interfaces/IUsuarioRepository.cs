﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Models;
using System.Collections.Generic;

namespace Recognize.Core.Interfaces
{
    public interface IUsuarioRepository
    {
        public List<Imagem> ListaImagem(int id);
        public Imagem EnviarImagem(Imagem model, int id);
        public Usuario AtualizarSenha(int id, Usuario usuario);
        public IActionResult ApagarFoto(int usuario, int imagem);
        public string RetornaLogin(int id);

    }
}
