﻿
using Recognize.Core.Models;
using System.Collections.Generic;

namespace Recognize.Core.Interfaces
{
    public interface IHomeRepository
    {
        public List<Imagem> ListaPublica();
        public object InfoGeral();
        public string Autenticar(string login, string password);
    }
}
