﻿using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Models;
using System.Collections.Generic;

namespace Recognize.Core.Interfaces
{
    public interface IGestorRepository
    {
        public string CriarUsuario(Usuario usuario);
        public IActionResult RemoverUsuario(int idUsuario);
        public List<Usuario> ListarUsuarios();
        public bool VerificaExistente(Usuario usuario);


    }
}
