﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Recognize.Core.Models;
using Recognize.Core.Services.Interfaces;

namespace Recognize.Api.Controllers
{
    [Authorize(Roles = "gestor")]
    [Route("api")]
    public class GestorController : ControllerBase
    {
        private readonly IGestorService _gestorService;

        public GestorController(IGestorService gestorService)
        {

            _gestorService = gestorService;

        }
        /// <summary>
        /// Recebe lista dos usuários cadastrados!
        /// </summary>
        /// <returns>Lista Usuários</returns>
        /// <response code="200"></response>
        /// <response code="401">"Token Inválido ou expirado!"</response>
        [HttpGet("gestor/list")]
        public ActionResult ObterUsuarios()
        {
            return Ok(_gestorService.ListarUsuarios());
        }

        /// <summary>
        /// Adiciona novos Usuários
        /// </summary>
        /// <param name="usuario"> Dados do usuário a ser cadastrado </param>
        /// <returns>Adicionar usuário</returns>
        /// <response code="200"></response>
        /// <response code="400">Se o usuário não for criado</response>
        /// <response code="401">"Token Inválido ou expirado!"</response>
        /// <response code="403">"Você não tem autorização para realizar esta ação"</response>
        [HttpPost("gestor/criar_usuario")]
        public IActionResult AdicionarUsuario([FromBody] Usuario usuario)
        {
            if (usuario == null)
            {
                return BadRequest("Dados incorretos, verifique");
            }
            return _gestorService.AdicionarUsuario(usuario);

        }

        /// <summary>
        /// Remove Usuário Informado e todos os dados relacionados a ele
        /// </summary>
        /// <param name="usuario"> Id do usuário a ser exclúido </param>
        /// <returns>Remover usuário</returns>
        /// <response code="200">Usuário exclúido</response>
        /// <response code="400">O usuário não foi excluido</response>
        /// <response code="401">"Token Inválido ou expirado!"</response>
        /// <response code="403">"Você não tem autorização para realizar esta ação"</response>
        [HttpDelete("gestor/remover_usuario/{usuario}")]
        public IActionResult RemoverUsuario([FromRoute] int usuario)
        {
            return _gestorService.RemoverUsuario(usuario);
        }
    }
}
