﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Recognize.Api.ViewModel;
using Recognize.Core.Models;
using Recognize.Core.Services.Interfaces;
using System.Collections.Generic;

namespace Recognize.Api.Controllers
{


    [Authorize(Roles = "usuario")]
    [Route("api")]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;

        public UsuarioController(IUsuarioService usuarioService)
        {

            _usuarioService = usuarioService;

        }

        /// <summary>
        /// Exibe lista de imagens de um único usuário
        /// </summary>
        /// <param name="id">Recupera o ID do usuário</param>
        /// <returns>Retorna uma lista de imagens do usuário</returns>
        /// <response code="200">Imagem analizada com sucesso</response>
        /// <response code="400">Usuario Inválido</response>
        /// <response code="401">"Token Inválido ou expirado!"</response>
        /// <response code="404">"Não existem imagens cadastradas!"</response>
        [HttpGet("usuario/{id}/list")]
        public IActionResult GetImagem([FromRoute] int id)
        {
            string loginUsuario = _usuarioService.RetornaLogin(id);

            if (User.Identity.Name != loginUsuario)
            {
                return BadRequest("Usuário incorreto / não existe");
            }
            var imagens = _usuarioService.ListaImagens(id);
            List<ViewModelImage> viewModels = new List<ViewModelImage>();
            if (imagens != null)
            {
                foreach (var item in imagens)
                {
                    viewModels.Add(new ViewModelImage
                    {
                        Id = item.Id,
                        Caminho = item.Caminho,
                        DataUpload = item.DataUpload,
                        Idade = item.Idade
                    });
                }
                return Ok(viewModels);
            }
            else
            {
                return NotFound("Não foram encontradas imagens!");
            }
        }

        /// <summary>
        /// Envia imagem para reconhecimento
        /// </summary>
        /// <returns>Detectar idade</returns>
        /// <param name="id">Id do usuário</param>
        /// <param name="caminho">Caminho da imagem</param>
        /// <response code="200">Imagem analizada com sucesso</response>
        /// <response code="401">"Token Inválido ou expirado!"</response>
        /// <response code="404">"Imagem Incorreta ou inexistente"</response>
        [HttpPost("usuario/{id}/enviar")]
        public IActionResult EnviarImagem([FromBody] Imagem caminho, [FromRoute] int id)
        {
            string loginUsuario = _usuarioService.RetornaLogin(id);

            if (User.Identity.Name != loginUsuario)
            {
                return BadRequest("Usuário incorreto / não existe");
            }
            Imagem obj = _usuarioService.EnviarImagem(caminho, id);
            if (obj != null)
            {
                ViewModelImage viewModelImage = new ViewModelImage
                {
                    Id = obj.Id,
                    Caminho = obj.Caminho,
                    DataUpload = obj.DataUpload,
                    Idade = obj.Idade
                };

                return Ok(viewModelImage);
            }
            else
            {
                return BadRequest("Imagem Inexistente ou incorreta");
            }
            

        }
        /// <summary>
        /// Operação para Atualizar senha do usuário
        /// </summary>
        /// <returns>Alterar senha</returns>
        /// <param name="id">Id do usuário</param>
        /// <param name="senha">Senha a ser atualizada</param>
        /// <response code="200">Senha atualizada</response>
        /// <response code="400">Usuario Inválido</response>
        /// <response code="401">"Token Inválido ou expirado!"</response>
        /// <response code="403">"Você não tem autorização para realizar esta ação"</response>
        [HttpPatch("usuario/{id}/atualizar")]
        public IActionResult Atualizar([FromRoute] int id, [FromBody] Usuario senha)
        {
            string loginUsuario = _usuarioService.RetornaLogin(id);

            if (User.Identity.Name != loginUsuario)
            {
                return BadRequest("Usuário incorreto / não existe");
            }

            return _usuarioService.AtualizaSenha(id, senha);

        }

        /// <summary>
        /// Remove imagem do usuário informado
        /// </summary>
        /// <returns>Remover imagem</returns>
        /// <param name="id">Id do usuário</param>
        /// <param name="imagem">Id da imagem que deseja ser exclúida</param>
        /// <response code="200">Imagem excluída</response>
        /// <response code="400">A imagem não foi excluida</response>
        /// <response code="401">"Token Inválido ou expirado!"</response>
        /// <response code="403">"Você não tem autorização para realizar esta ação"</response>
        /// <response code="404">"Imagem não encontrada"</response>
        [HttpDelete("usuario/{id}/delete/{imagem}")]
        public IActionResult ApagarImagem([FromRoute] int id, [FromRoute] int imagem)
        {
            string loginUsuario = _usuarioService.RetornaLogin(id);

            if (User.Identity.Name != loginUsuario)
            {
                return BadRequest("Usuário incorreto / não existe");
            }

            return _usuarioService.ApagarImagem(id, imagem);
        }


    }
}