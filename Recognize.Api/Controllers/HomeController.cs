﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Recognize.Api.ViewModel;
using Recognize.Core.Models;
using Recognize.Core.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recognize.Api.Controllers
{
    [Route("api")]
    public class HomeController : ControllerBase
    {
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;

        }
        /// <summary>
        /// Login
        /// </summary>
        /// <returns>Login</returns>
        /// <param name="usuario">usuario e senha para login</param>
        /// <response code="200">Token enviado</response>
        /// <response code="401">Usuario ou senha incorretos</response>
        /// <response code="404">"Usuário ou senha incorretos"</response>
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Autenticar([FromBody] Usuario usuario)
        {
            var resultado = _homeService.Autenticar(usuario);
            if (resultado.Result == null)
            {
                return NotFound("Usuário ou senha incorreta");
            }
            return resultado.Result.Value;
        }

        /// <summary>
        /// Envia lista das 3 imagens mais recentes enviadas para a API
        /// </summary>
        /// <returns>Lista Imagens Recentes</returns>
        /// <response code="200">Lista Exibida</response>
        [HttpGet]
        [Route("publico")]
        [AllowAnonymous]
        public ActionResult ListaPublica()
        {
            var listaImagem = _homeService.ListaPublica();
            List<ViewModelImage> viewModel = new List<ViewModelImage>();
            foreach (var item in listaImagem)
            {
                viewModel.Add(new ViewModelImage
                {
                    Id = item.Id,
                    Idade = item.Idade,
                    DataUpload = item.DataUpload,
                    Caminho = item.Caminho
                    
                });
            }
            return Ok(viewModel);
            
        }

        /// <summary>
        /// Informações sobre a quantidade de usuários e imagens cadastrados na API
        /// </summary>
        /// <returns>Informações gerais</returns>
        /// <response code="200">Lista Exibida</response>
        [HttpGet]
        [Route("info")]
        [AllowAnonymous]
        public ActionResult InfoGeral()
        {
            return Ok(_homeService.InfoGeral());
        }


    }
}