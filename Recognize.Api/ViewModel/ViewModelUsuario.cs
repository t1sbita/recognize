﻿using System;

namespace Recognize.Api.ViewModel
{
    public class ViewModelUsuario
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public DateTime DataUpload { get; set; }
    }
}
