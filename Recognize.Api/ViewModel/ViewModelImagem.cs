﻿using System;

namespace Recognize.Api.ViewModel
{
    public class ViewModelImage
    {
        public int Id { get; set; }
        public DateTime DataUpload { get; set; }
        public string Caminho { get; set; }
        public int Idade { get; set; }
    }
}
